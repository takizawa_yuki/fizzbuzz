package fizzbuzz;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/Fizzbuzz"})
public class Fizzbuzz extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.getRequestDispatcher("/fizzbuzz.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if(!request.getParameter("int").matches("^[0-9]+$")){
			request.getRequestDispatcher("/fizzbuzz.jsp").forward(request, response);
			return;
		}

		int a = Integer.parseInt(request.getParameter("int"));
		String s = null;

		if(a % 15 == 0){
			s = "FizzBuzz";
			request.setAttribute("checkint", s);
		}else if(a % 3 == 0){
			s = "Fizz";
			request.setAttribute("checkint", s);
		}else if(a % 5 == 0){
			s = "Buzz";
			request.setAttribute("checkint", s);
		}else{
			request.setAttribute("checkint", a);
		}

		request.getRequestDispatcher("/fizzbuzz.jsp").forward(request, response);
	}

}
